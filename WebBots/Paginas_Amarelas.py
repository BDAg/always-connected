import requests
import pymongo
from bs4 import BeautifulSoup

endereco_site='http://www.brasilpaginasamarelas.com.br/busca/browse_locations.php'
contador=0

class coletarContato():
    def __init__(self):
        self.conectandoMongo()
        self.coletarInformacoes()


    def conectandoMongo(self):
        self.conexao=pymongo.MongoClient('mongodb://localhost:27017/')
        self.data_base=self.conexao['Contatos']


    def coletarInformacoes(self):
        req_principal=requests.get(endereco_site)
        soup_principal=BeautifulSoup(req_principal.content, 'html.parser')
        
        for localizacao in soup_principal.select('body > div.container > div > div.col-xl-9.col-lg-9.col-md-8.col-sm-12.col-xs-12 > div.row > div > div > div > h4 > a'):
            print(f'Script|Estado: {localizacao.text}')
            req_categoria=requests.get(localizacao['href'])
            soup_categoria=BeautifulSoup(req_categoria.content, 'html.parser')   
            

            for categoria in soup_categoria.select('h4.media-heading > a'):
                categoria_formatado=str(categoria.text).upper().strip()
                req_lista=requests.get(categoria['href'])
                soup_lista=BeautifulSoup(req_lista.content, 'html.parser')
                
                for contato in soup_lista.select('div.panel-body > h4 > a'):
                    req_contato=requests.get(contato['href'])
                    soup_contato=BeautifulSoup(req_contato.content, 'html.parser')

                    nome=soup_contato.find('span',{'itemprop':'name'})
                    nome_formatado=str(nome.text).upper().strip()

                    telefone=soup_contato.find('span',{'itemprop':'telephone'})
                    telefone_formatado=str(telefone.text).upper().strip()

                    rua=soup_contato.find('span',{'itemprop':'streetAddress'})
                    rua_formatado=str(rua.text).upper().strip().replace('\n','').replace('  ','')
                    bairro=soup_contato.find('span',{'itemprop':'addressLocality'})
                    bairro_formatado=str(bairro.text).upper().strip().replace('\n','').replace('  ','')           
                    cidade=soup_contato.find('span',{'itemprop':'addressRegion'})
                    cidade_formatado=str(cidade.text).upper().strip().replace('\n','').replace('  ','')
                    endereco_formatado=f'{rua_formatado}, {bairro_formatado} - {cidade_formatado}'

                    self.salvarInformacoes(nome_formatado, telefone_formatado, endereco_formatado, categoria_formatado)


    def salvarInformacoes(self, nomef, telefonef, enderecof, categoriaf):
        global contador
        busca=self.data_base.Lista_Contatos.find_one({
            'Categoria': categoriaf,
            'Nome': nomef,
            'Endereço': enderecof,
            'Telefone': telefonef
        })

        if busca == None:
            self.data_base.Lista_Contatos.insert_one({
            'Categoria': categoriaf,
            'Nome': nomef,
            'Endereço': enderecof,
            'Telefone': telefonef
        })
            contador=contador+1

coletarContato()

print(f'MongoDB| Foram adicionados {contador} registros ao banco de dados')
