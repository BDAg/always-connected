import requests
from bs4 import BeautifulSoup
import pymongo

endereco_site='http://www.ocnet.com.br/lista/'
endereco_complementar='http://www.ocnet.com.br/'
lista_nomes=[]
lista_enderecos=[]
lista_telefones=[]
lista_categorias=[]

class coletarContato():
    def __init__(self):
        global lista_nomes
        global lista_enderecos
        global lista_telefones
        global lista_categorias
        
        self.conectandoMongo()
        self.coletarInformacao(lista_nomes, lista_enderecos, lista_telefones, lista_categorias)
        print('\n\n')
        self.salvandoInformacao(lista_nomes, lista_enderecos, lista_telefones, lista_categorias)


    def conectandoMongo(self):
        self.conexao=pymongo.MongoClient('mongodb://localhost:27017/')
        self.data_base=self.conexao['Contatos']

    def coletarInformacao(self, lista_nomes, lista_enderecos, lista_telefones, lista_categorias):
        global endereco_site
        global endereco_complementar
        req_principal=requests.get(endereco_site)
        soup_principal=BeautifulSoup(req_principal.content, 'html.parser')
        
        for x in soup_principal.select('#cat-lista-busca > li > a'):
            categoria=str(x.text).upper().strip()
            print(f'Script| Coletando dados da categoria: {categoria}!')
            
            req_categoria=requests.get(endereco_complementar+x['href'])
            soup_categoria=BeautifulSoup(req_categoria.content, 'html.parser')
            
            for x in range(len(soup_categoria.select('#lista-telefones > li'))):
                lista_categorias.append(categoria)
            
            for nome in soup_categoria.select('#lista-telefones > li > div > div > h3'):
                nome_formatado=str(nome.text).upper().strip()
                lista_nomes.append(nome_formatado)
            
            for endereco in soup_categoria.select('#lista-telefones > li > div > div > p.endereco'):
                endereco_formatado=str(endereco.text).upper().strip().replace('\n',', ').replace('  ','').replace('"','').replace('\t','')
                lista_enderecos.append(endereco_formatado)

            for telefone in soup_categoria.select('#lista-telefones > li > div > div > p.telefone > span'):
                telefone_formatado=str(telefone.text).upper().strip().replace('\n','').replace('  ','').replace('"','').replace('\t','')
                lista_telefones.append(telefone_formatado)

    def salvandoInformacao(self, lista_nomes, lista_enderecos, lista_telefones, lista_categorias):
        contador=0
        for nome, endereco, telefone, categoria in zip (lista_nomes, lista_enderecos, lista_telefones, lista_categorias):
            busca=self.data_base.Lista_Contatos.find_one({
                'Categoria': categoria,
                'Nome': nome,
                'Endereço': endereco,
                'Telefone': telefone
            })

            if busca == None:
                self.data_base.Lista_Contatos.insert_one({
                'Categoria': categoria,
                'Nome': nome,
                'Endereço': endereco,
                'Telefone': telefone
            })
                contador=contador+1

        print(f'MongoDB| Foram adicionados {contador} registros ao banco de dados')

coletarContato()