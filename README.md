# ALWAYS CONNECTED

O projeto Always Connected define-se como um WebBot que busca fontes de contato na internet através de palavras chave.


<h2>Utilização</h2>
<p>Para a utilização do Always Connected é necessário que haja as seguintes ferramentas instaladas na máquina:</p>
[Python](https://www.python.org/)<br>
[Requests](https://pypi.org/project/requests/)<br>
[BeautifulSoup](https://pypi.org/project/beautifulsoup4/)<br>
[Pymongo](https://pypi.org/project/pymongo/)<br>
[MongoDB](https://www.mongodb.com/)<br>
<br>
<p>Para o funcionamento do programa e armazenagem das fontes de contato, é necessário que seu servidor local do MongoDB esteja aberto.</p>
<br>
<p>Para maiores informações como documentação, participantes e ferramentas utilizadas visite nossa [Wiki](https://gitlab.com/BDAg/always-connected/-/wikis/Wiki).</p>